using System.Threading.Tasks;
using SecureFlight.Core.Entities;

namespace SecureFlight.Core.Interfaces;

public interface IAirportService : IService<Airport>
{
    Task<OperationResult<Airport>> Update(Airport airport);
}