using System.Threading;
using System.Threading.Tasks;

namespace SecureFlight.Core.Interfaces;

public interface IUnitOfWork
{
    Task<int> CommitChanges(CancellationToken cancellationToken = default);
}