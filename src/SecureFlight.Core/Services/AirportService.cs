using System.Threading.Tasks;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using System.Linq;

namespace SecureFlight.Core.Services;

public class AirportService : BaseService<Airport>, IAirportService
{
    public AirportService(IUnitOfWork unitOfWork, IRepository<Airport> repository) : base(unitOfWork, repository)
    {
    }

    public async Task<OperationResult<Airport>> Update(Airport airport)
    {

        if (!await Repository.Exists(a => a.Code == airport.Code))
        {
            return new OperationResult<Airport>(new Error()
            {
                Code = ErrorCode.NotFound,
                Message = $"Airport {airport.Code} not found",
            });
        }

        Repository.Update(airport);
        await UnitOfWork.CommitChanges();

        return new OperationResult<Airport>(airport);
    }

}
