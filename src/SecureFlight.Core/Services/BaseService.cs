﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Core.Services;

public class BaseService<TEntity> : IService<TEntity>
    where TEntity : class
{
    protected IUnitOfWork UnitOfWork { get; private set; }
    protected IRepository<TEntity> Repository { get; private set; }

    public BaseService(IUnitOfWork unitOfWork, IRepository<TEntity> repository)
    {
        UnitOfWork = unitOfWork;
        Repository = repository;
    }
    public async Task<OperationResult<IReadOnlyList<TEntity>>> GetAllAsync()
    {
        return new OperationResult<IReadOnlyList<TEntity>>(await Repository.GetAllAsync());
    }
}