using System.Threading;
using System.Threading.Tasks;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Infrastructure;

public class UnitOfWork : IUnitOfWork
{
    private SecureFlightDbContext _context;

    public UnitOfWork(SecureFlightDbContext context){
        _context = context;
    }

    public Task<int> CommitChanges(CancellationToken cancellationToken = default) => _context.SaveChangesAsync(cancellationToken);
  
}